<?php

namespace App\DataFixtures;

use App\Entity\Utilisateurs;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new Utilisateurs();
        $user->setUsername('User');
        $user->setAdresse('Rue de luser');
        $user->setEmail('email@usermarkgmail.com');
        $user->addRole('ROLE_USER');
        $user->setPassword($this->encoder->encodePassword($user, 'azer'));
        $manager->persist($user);
        $manager->flush();
    }
    public static function getGroups(): array
    {
        return ['group1', 'group2'];
    }
}
