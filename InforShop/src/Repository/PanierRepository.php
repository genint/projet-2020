<?php

namespace App\Repository;

use App\Entity\Panier;
use App\Entity\Produits;
use App\Entity\Utilisateurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * @method Panier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Panier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Panier[]    findAll()
 * @method Panier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PanierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Panier::class);
    }

    public function findPanierUser(Utilisateurs $utilisateurs)
    {
        $id = $utilisateurs->getId();
        return $this->createQueryBuilder('p')
            ->where('p.panierutilisateurs = :id')
            ->setParameter("id", $id)
            ->getQuery()
            ->getResult();
    }
    /**
     * @return Panier
     */
    public function findPanierFromId(int $id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter("id", $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    /**
     * @return Produits
     */
    public function findProduitsInPanier(int $id)
    {
        return $this->createQueryBuilder('pr')
            ->select('pr.')
            ->join('pr.panierproduits', 'pp')
            ->where('u.produitspanier.produitsid = :id')
            ->setParameter("id", $id)
            ->getQuery()
            ->getResult();
    }
}