<?php

namespace App\Repository;

use App\Entity\Panier;
use App\Entity\Produits;
use App\Entity\ProduitsSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ObjectManager;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

/**
 * @method Produits|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produits|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produits[]    findAll()
 * @method Produits[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitsRepository extends ServiceEntityRepository
{
    /**
     * @var ObjectManager
     */
    private $em;
    public function __construct(ManagerRegistry $registry,EntityManagerInterface $em)
    {
        parent::__construct($registry, Produits::class);
        $this->em = $em;
    }

    /**
     * @return Query
     */
    public function findAllVisibleQuery(ProduitsSearch $search): Query
    {
        $query = $this->findVisibleQuery();

        if ($search->getMaxPrix())  {
            $query  = $query
                ->andWhere('p.prix <= :maxprix')
                ->setParameter('maxprix', $search->getMaxPrix());
        }

        if ($search->getMinPrix())  {
            $query  = $query
                ->andWhere('p.prix >= :minprix')
                ->setParameter('minprix', $search->getMinPrix());
        }
        return $query->getQuery();
    }

    public function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->where('p.id >= 1');
    }

    /**
     * @return Produits
     */
    public function findById($id)
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter("id", $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findNewestProducts()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.updated_at', 'DESC')->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }

    public function findMostSoldProducts()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.quantite', 'DESC')->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }


    public function findProduitsInPanier(int $id)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Produits::class, 'p');
        $rsm->addFieldResult('p', 'quantite', 'quantite');
        $rsm->addFieldResult('p', 'prix', 'prix');
        $rsm->addFieldResult('p', 'marque', 'marque');
        $rsm->addFieldResult('p', 'categorie', 'categorie');
        $rsm->addFieldResult('p', 'nom', 'nom');
        $rsm->addFieldResult('p', 'image', 'image');
        $rsm->addFieldResult('p', 'filename', 'filename');
        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'updated_at', 'updated_at');

        $query = $this->em->createNativeQuery('SELECT pr.quantite,prix,marque,categorie,nom,image,filename,pr.id,updated_at FROM produits AS pr LEFT JOIN produits_panier pp on pr.id = pp.produits_id LEFT JOIN panier p on p.id = pp.panier_id WHERE p.id = ?', $rsm);
        $query->setParameter(1, $id);

        $produits = $query->getResult();
        return $produits;
    }




    // /**
    //  * @return Produits[] Returns an array of Produits objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produits
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
