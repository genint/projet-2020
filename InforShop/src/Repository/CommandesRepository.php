<?php

namespace App\Repository;

use App\Entity\Commandes;
use App\Entity\Produits;
use App\Entity\Utilisateurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Commandes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commandes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commandes[]    findAll()
 * @method Commandes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commandes::class);
    }

    public function findUserCommands(Utilisateurs $user)
    {
        $id = $user->getId();
        return $this->createQueryBuilder('p')
            ->where('p.commandesutilisateurs = :id')
            ->setParameter("id", $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Produits
     */
    public function findProductsFromCommand(Commandes $commandes)
    {
        return $this->createQueryBuilder('p')
            ->join('p.commandesproduits', 'u')
            ->where('$u.produitscommandes.commandesid = :id')
            ->setParameter("id", $commandes->getId())
            ->getQuery()
            ->getResult();
    }
    public function findProductsQuantFromCommand(Commandes $commandes)
    {
        return $this->createQueryBuilder('p')
            ->select('u.quantite')
            ->join('p.commandesproduits', 'u')
            ->where('$u.produitscommandes.commandesid = :id')
            ->setParameter("id", $commandes->getId())
            ->getQuery()
            ->getResult();
    }
    // /**
    //  * @return Commandes[] Returns an array of Commandes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commandes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
