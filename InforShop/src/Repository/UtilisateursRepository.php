<?php

namespace App\Repository;

use App\Entity\Commandes;
use App\Entity\Panier;
use App\Entity\Utilisateurs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Utilisateurs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utilisateurs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utilisateurs[]    findAll()
 * @method Utilisateurs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtilisateursRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateurs::class);
    }

    public function findUserUsername(String $username): Utilisateurs
    {
        return $this->createQueryBuilder('p')
            ->where('p.username = :username')
            ->setParameter("username", $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Commandes
     */
    public function findCommandsFromUser(Utilisateurs $user)
    {
        return $this->createQueryBuilder('p')
            ->join('p.utilisateurscommandes', 'u')
            ->where('u.commandesutilisateurs = :id')
            ->setParameter("id", $user->getId())
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return Utilisateurs[] Returns an array of Utilisateurs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Utilisateurs
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
