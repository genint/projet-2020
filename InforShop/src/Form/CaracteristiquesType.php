<?php

namespace App\Form;

use App\Entity\Caracteristiques;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaracteristiquesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero_modele')
            ->add('image')
            ->add('serie')
            ->add('couleur')
            ->add('garantie_constructeur')
            ->add('systeme_exploitation')
            ->add('descrption_clavier')
            ->add('marque_processeur')
            ->add('type_processeur')
            ->add('vitesse_processeur')
            ->add('nbr_coeurs')
            ->add('taille_memoire_vive')
            ->add('taille_disque_dur')
            ->add('technologie_disque_dur')
            ->add('type_ecran')
            ->add('taille_ecran')
            ->add('res_ecran')
            ->add('res_max_ecran')
            ->add('descrip_carte_graphique')
            ->add('GPU')
            ->add('GPU_ram')
            ->add('type_ram_carte_graphique')
            ->add('connectivite')
            ->add('bluetooth')
            ->add('nbr_ports_HDMI')
            ->add('nbr_ports_VGA')
            ->add('nbr_ports_USB2')
            ->add('nbr_ports_USB3')
            ->add('nbr_ports_ethernet')
            ->add('type_connecteur')
            ->add('dimensions')
            ->add('poids')
            ->add('taille_cable')
            ->add('divers')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Caracteristiques::class,
        ]);
    }
}
