<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

class CSVFile
{
    /**
     * @var File|null
     * @Vich\UploadableField(mapping="produits_images", fileNameProperty="csvFileName")
     */
    private $csvFile;

    /**
     * @var string
     */
    private $csvFileName;

    /**
     * @return File|null
     */
    public function getCsvFile(): ?File
    {
        return $this->csvFile;
    }

    /**
     * @param File|null $csvFile
     * @return CSVFile
     */
    public function setCsvFile(?File $csvFile): CSVFile
    {
        $this->csvFile = $csvFile;
        return $this;
    }

    /**
     * @return string
     */
    public function getCsvFileName(): string
    {
        return $this->csvFileName;
    }

    /**
     * @param string $csvFileName
     * @return CSVFile
     */
    public function setCsvFileName(string $csvFileName): CSVFile
    {
        $this->csvFileName = $csvFileName;
        return $this;
    }
}