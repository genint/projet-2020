<?php

namespace App\Entity;

use App\Repository\PromotionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromotionsRepository::class)
 */
class Promotions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $remise;

    /**
     * @ORM\Column(type="integer")
     */
    private $delai;

    /**
     * @ORM\OneToMany(targetEntity=Produits::class, mappedBy="produitspromotions", cascade={"persist", "remove"})
     */
    private $promotionsproduits;

    public function __construct()
    {
        $this->promotionsproduits = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    public function getRemise(): ?int
    {
        return $this->remise;
    }

    public function setRemise(int $remise): self
    {
        $this->remise = $remise;

        return $this;
    }

    public function getDelai(): ?int
    {
        return $this->delai;
    }

    public function setDelai(int $delai): self
    {
        $this->delai = $delai;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getPromotionsproduits(): Collection
    {
        return $this->promotionsproduits;
    }

    public function addPromotionsproduit(Produits $promotionsproduit): self
    {
        if (!$this->promotionsproduits->contains($promotionsproduit)) {
            $this->promotionsproduits[] = $promotionsproduit;
            $promotionsproduit->setProduitspromotions($this);
        }

        return $this;
    }

    public function removePromotionsproduit(Produits $promotionsproduit): self
    {
        if ($this->promotionsproduits->removeElement($promotionsproduit)) {
            // set the owning side to null (unless already changed)
            if ($promotionsproduit->getProduitspromotions() === $this) {
                $promotionsproduit->setProduitspromotions(null);
            }
        }

        return $this;
    }

}
