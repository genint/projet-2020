<?php

namespace App\Entity;

use App\Repository\UtilisateursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UtilisateursRepository::class)
 */
class Utilisateurs implements UserInterface,\Serializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @Assert\Length(max=250)
     */
    private $plainPassword;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Commandes::class, mappedBy="commandesutilisateurs", cascade={"persist", "remove"})
     */
    private $utilisateurscommandes;

    /**
     * @ORM\OneToOne (targetEntity=Panier::class, mappedBy="panierutilisateurs", cascade={"persist", "remove"})
     */
    private $utilisateurspanier;

    /**
     * @ORM\Column (name="roles", type="array", nullable=true)
     */
    private $roles = array();

    public function getRoles(){
        if (empty($this->roles)){
            return ['ROLE_USER'];
        }
        return $this->roles;
    }

    public function addRole($role){
        $this->roles[]=$role;
    }
    public function RemoveRole($role){
        $this->roles[]=empty(true);
    }


    public function __construct()
    {
        $this->utilisateurscommandes = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }
    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     * @return Utilisateurs
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password
        ]);
    }

    public function unserialize($serialized)
    {
        list(
        $this->id,
        $this->username,
        $this->password) = unserialize($serialized, ['allowed_classes' =>false]);
    }

    /**
     * @return Collection|Commandes[]
     */
    public function getUtilisateurscommandes(): Collection
    {
        return $this->utilisateurscommandes;
    }

    public function addUtilisateurscommande(Commandes $utilisateurscommande): self
    {
        if (!$this->utilisateurscommandes->contains($utilisateurscommande)) {
            $this->utilisateurscommandes[] = $utilisateurscommande;
            $utilisateurscommande->setCommandesutilisateurs($this);
        }

        return $this;
    }

    public function removeUtilisateurscommande(Commandes $utilisateurscommande): self
    {
        if ($this->utilisateurscommandes->removeElement($utilisateurscommande)) {
            // set the owning side to null (unless already changed)
            if ($utilisateurscommande->getCommandesutilisateurs() === $this) {
                $utilisateurscommande->setCommandesutilisateurs(null);
            }
        }

        return $this;
    }

    public function getUtilisateurpanier(): ?Panier
    {
        return $this->utilisateurspanier;
    }
    public function setUtilisateurspanier(?Panier $utilisateurspanier): self
    {
        $this->utilisateurspanier = $utilisateurspanier;

        // set (or unset) the owning side of the relation if necessary
        $newUtilisateurspanier = null === $utilisateurspanier ? null : $this;
        if ($utilisateurspanier->getPanierutilisateurs() !== $newUtilisateurspanier) {
            $utilisateurspanier->setPanierutilisateurs($newUtilisateurspanier);
        }

        return $this;
    }

    public function setProduitscaracteristiques(?Caracteristiques $produitscaracteristiques): self
    {
        $this->produitscaracteristiques = $produitscaracteristiques;

        return $this;
    }

}
