<?php

namespace App\Entity;

use App\Repository\CommandesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandesRepository::class)
 */
class Commandes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $etat;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_commande;

    /**
     * @ORM\Column(type="binary")
     */
    private $paiement;

    /**
     * @ORM\ManyToMany(targetEntity=Produits::class, mappedBy="produitscommandes", cascade={"persist", "remove"})
     */
    private $commandesproduits;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateurs::class, inversedBy="utilisateurscommandes", cascade={"persist", "remove"})
     */
    private $commandesutilisateurs;

    public function __construct()
    {
        $this->commandesproduits = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getDateCommande(): ?\DateTimeInterface
    {
        return $this->date_commande;
    }

    public function setDateCommande(\DateTimeInterface $date_commande): self
    {
        $this->date_commande = $date_commande;

        return $this;
    }

    public function getPaiement()
    {
        return $this->paiement;
    }

    public function setPaiement($paiement): self
    {
        $this->paiement = $paiement;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getCommandesproduits(): Collection
    {
        return $this->commandesproduits;
    }

    public function addCommandesproduit(Produits $commandesproduit): self
    {
        if (!$this->commandesproduits->contains($commandesproduit)) {
            $this->commandesproduits[] = $commandesproduit;
            $commandesproduit->addProduitscommande($this);
        }

        return $this;
    }

    public function removeCommandesproduit(Produits $commandesproduit): self
    {
        if ($this->commandesproduits->removeElement($commandesproduit)) {
            $commandesproduit->removeProduitscommande($this);
        }

        return $this;
    }

    public function getCommandesutilisateurs(): ?Utilisateurs
    {
        return $this->commandesutilisateurs;
    }

    public function setCommandesutilisateurs(?Utilisateurs $commandesutilisateurs): self
    {
        $this->commandesutilisateurs = $commandesutilisateurs;

        return $this;
    }

}
