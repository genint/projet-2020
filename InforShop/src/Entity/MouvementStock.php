<?php

namespace App\Entity;

use App\Repository\MouvementStockRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MouvementStockRepository::class)
 */
class MouvementStock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite_mouv;

    /**
     * @ORM\Column(type="integer")
     */
    private $commentaire;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_mouv;



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getQuantiteMouv(): ?int
    {
        return $this->quantite_mouv;
    }

    public function setQuantiteMouv(int $quantite_mouv): self
    {
        $this->quantite_mouv = $quantite_mouv;

        return $this;
    }

    public function getCommentaire(): ?int
    {
        return $this->commentaire;
    }

    public function setCommentaire(int $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getDateMouv(): ?\DateTimeInterface
    {
        return $this->date_mouv;
    }

    public function setDateMouv(\DateTimeInterface $date_mouv): self
    {
        $this->date_mouv = $date_mouv;

        return $this;
    }

}
