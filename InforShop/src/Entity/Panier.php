<?php

namespace App\Entity;

use App\Entity\Produits;
use App\Repository\PanierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=PanierRepository::class)
 * @Vich\Uploadable()
 */
class Panier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\ManyToMany(targetEntity=Produits::class, mappedBy="produitspanier", cascade={"persist", "remove"})
     */
    private $panierproduits;

    /**
     * @ORM\OneToOne(targetEntity=Utilisateurs::class, inversedBy="utilisateurspanier", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $panierutilisateurs;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite() :?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getPanierproduits(): Collection
    {
        return $this->panierproduits;
    }

    public function addPanierproduits(Produits $panierproduit): self
    {
            $this->panierproduits[] = $panierproduit;
            $panierproduit->addProduitspanier($this);

        return $this;
    }

    public function removePanierproduits(Produits $panierproduits): self
    {
        if ($this->panierproduits->removeElement($panierproduits)) {
            $panierproduits->removeProduitspanier($this);
        }

        return $this;
    }

    public function getPanierutilisateurs(): ?Utilisateurs
    {
        return $this->panierutilisateurs;
    }

    public function setPanierutilisateurs(?Utilisateurs $panierutilisateur): self
    {
        $this->panierutilisateurs = $panierutilisateur;

        return $this;
    }
}