<?php
namespace App\Entity;

class ProduitsSearch {

    /**
     * @var int|null
     */
    private $maxPrix;

    /**
     * @var int|null
     */
    private $minPrix;

    /**
     * @return int|null
     */
    public function getMaxPrix(): ?int
    {
        return $this->maxPrix;
    }

    /**
     * @param int|null $maxPrix
     * @return ProduitsSearch
     */
    public function setMaxPrix(int $maxPrix): ProduitsSearch
    {
        $this->maxPrix = $maxPrix;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinPrix(): ?int
    {
        return $this->minPrix;
    }

    /**
     * @param int|null $minPrix
     * @return ProduitsSearch
     */
    public function setMinPrix(int $minPrix): ProduitsSearch
    {
        $this->minPrix = $minPrix;
        return $this;
    }

}