<?php

namespace App\Entity;

use App\Repository\CaracteristiquesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CaracteristiquesRepository::class)
 */
class Caracteristiques
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_modele;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $serie;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $garantie_constructeur;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $systeme_exploitation;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $descrption_clavier;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $marque_processeur;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $type_processeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vitesse_processeur;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_coeurs;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $taille_memoire_vive;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $taille_disque_dur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $technologie_disque_dur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeEcran;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tailleEcran;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resEcran;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resMaxEcran;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descrip_carte_graphique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $GPU;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $GPU_ram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_ram_carte_graphique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $connectivite;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $bluetooth;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_ports_HDMI;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_ports_VGA;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_ports_USB2;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_ports_USB3;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $nbr_ports_ethernet;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeConnecteur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dimensions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $poids;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tailleCable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $divers;

    /**
     * @ORM\OneToOne(targetEntity=Produits::class, mappedBy="produitscaracteristiques", cascade={"persist", "remove"})
     *
     */
    private $caracteristiquesproduits;




    public function getNumeroModele(): ?string
    {
        return $this->numero_modele;
    }

    public function setNumeroModele(string $numero_modele): self
    {
        $this->numero_modele = $numero_modele;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getGarantieConstructeur(): ?string
    {
        return $this->garantie_constructeur;
    }

    public function setGarantieConstructeur(?string $garantie_constructeur): self
    {
        $this->garantie_constructeur = $garantie_constructeur;

        return $this;
    }

    public function getSystemeExploitation(): ?string
    {
        return $this->systeme_exploitation;
    }

    public function setSystemeExploitation(?string $systeme_exploitation): self
    {
        $this->systeme_exploitation = $systeme_exploitation;

        return $this;
    }

    public function getDescrptionClavier(): ?string
    {
        return $this->descrption_clavier;
    }

    public function setDescrptionClavier(?string $descrption_clavier): self
    {
        $this->descrption_clavier = $descrption_clavier;

        return $this;
    }

    public function getMarqueProcesseur(): ?string
    {
        return $this->marque_processeur;
    }

    public function setMarqueProcesseur(string $marque_processeur): self
    {
        $this->marque_processeur = $marque_processeur;

        return $this;
    }

    public function getTypeProcesseur(): ?string
    {
        return $this->type_processeur;
    }

    public function setTypeProcesseur(string $type_processeur): self
    {
        $this->type_processeur = $type_processeur;

        return $this;
    }

    public function getVitesseProcesseur(): ?string
    {
        return $this->vitesse_processeur;
    }

    public function setVitesseProcesseur(string $vitesse_processeur): self
    {
        $this->vitesse_processeur = $vitesse_processeur;

        return $this;
    }

    public function getNbrCoeurs(): ?int
    {
        return $this->nbr_coeurs;
    }

    public function setNbrCoeurs(?int $nbr_coeurs): self
    {
        $this->nbr_coeurs = $nbr_coeurs;

        return $this;
    }

    public function getTailleMemoireVive(): ?string
    {
        return $this->taille_memoire_vive;
    }

    public function setTailleMemoireVive(string $taille_memoire_vive): self
    {
        $this->taille_memoire_vive = $taille_memoire_vive;

        return $this;
    }

    public function getTailleDisqueDur(): ?string
    {
        return $this->taille_disque_dur;
    }

    public function setTailleDisqueDur(?string $taille_disque_dur): self
    {
        $this->taille_disque_dur = $taille_disque_dur;

        return $this;
    }

    public function getTechnologieDisqueDur(): ?string
    {
        return $this->technologie_disque_dur;
    }

    public function setTechnologieDisqueDur(?string $technologie_disque_dur): self
    {
        $this->technologie_disque_dur = $technologie_disque_dur;

        return $this;
    }

    public function getTypeEcran(): ?string
    {
        return $this->typeEcran;
    }

    public function setTypeEcran(?string $typeEcran): self
    {
        $this->typeEcran = $typeEcran;

        return $this;
    }

    public function getTailleEcran(): ?string
    {
        return $this->tailleEcran;
    }

    public function setTailleEcran(?string $tailleEcran): self
    {
        $this->tailleEcran = $tailleEcran;

        return $this;
    }

    public function getResEcran(): ?string
    {
        return $this->resEcran;
    }

    public function setResEcran(?string $resEcran): self
    {
        $this->resEcran = $resEcran;

        return $this;
    }

    public function getResMaxEcran(): ?string
    {
        return $this->resMaxEcran;
    }

    public function setResMaxEcran(?string $resMaxEcran): self
    {
        $this->resMaxEcran = $resMaxEcran;

        return $this;
    }

    public function getDescripCarteGraphique(): ?string
    {
        return $this->descrip_carte_graphique;
    }

    public function setDescripCarteGraphique(?string $descrip_carte_graphique): self
    {
        $this->descrip_carte_graphique = $descrip_carte_graphique;

        return $this;
    }

    public function getGPU(): ?string
    {
        return $this->GPU;
    }

    public function setGPU(?string $GPU): self
    {
        $this->GPU = $GPU;

        return $this;
    }

    public function getGPURam(): ?string
    {
        return $this->GPU_ram;
    }

    public function setGPURam(?string $GPU_ram): self
    {
        $this->GPU_ram = $GPU_ram;

        return $this;
    }

    public function getTypeRamCarteGraphique(): ?string
    {
        return $this->type_ram_carte_graphique;
    }

    public function setTypeRamCarteGraphique(?string $type_ram_carte_graphique): self
    {
        $this->type_ram_carte_graphique = $type_ram_carte_graphique;

        return $this;
    }

    public function getConnectivite(): ?string
    {
        return $this->connectivite;
    }

    public function setConnectivite(?string $connectivite): self
    {
        $this->connectivite = $connectivite;

        return $this;
    }

    public function getBluetooth(): ?string
    {
        return $this->bluetooth;
    }

    public function setBluetooth(?string $bluetooth): self
    {
        $this->bluetooth = $bluetooth;

        return $this;
    }

    public function getNbrPortsHDMI(): ?int
    {
        return $this->nbr_ports_HDMI;
    }

    public function setNbrPortsHDMI(?int $nbr_ports_HDMI): self
    {
        $this->nbr_ports_HDMI = $nbr_ports_HDMI;

        return $this;
    }

    public function getNbrPortsVGA(): ?int
    {
        return $this->nbr_ports_VGA;
    }

    public function setNbrPortsVGA(?int $nbr_ports_VGA): self
    {
        $this->nbr_ports_VGA = $nbr_ports_VGA;

        return $this;
    }

    public function getNbrPortsUSB2(): ?int
    {
        return $this->nbr_ports_USB2;
    }

    public function setNbrPortsUSB2(?int $nbr_ports_USB2): self
    {
        $this->nbr_ports_USB2 = $nbr_ports_USB2;

        return $this;
    }

    public function getNbrPortsUSB3(): ?int
    {
        return $this->nbr_ports_USB3;
    }

    public function setNbrPortsUSB3(?int $nbr_ports_USB3): self
    {
        $this->nbr_ports_USB3 = $nbr_ports_USB3;

        return $this;
    }

    public function getNbrPortsEthernet(): ?int
    {
        return $this->nbr_ports_ethernet;
    }

    public function setNbrPortsEthernet(?int $nbr_ports_ethernet): self
    {
        $this->nbr_ports_ethernet = $nbr_ports_ethernet;

        return $this;
    }

    public function getTypeConnecteur(): ?string
    {
        return $this->typeConnecteur;
    }

    public function setTypeConnecteur(?string $typeConnecteur): self
    {
        $this->typeConnecteur = $typeConnecteur;

        return $this;
    }

    public function getDimensions(): ?string
    {
        return $this->dimensions;
    }

    public function setDimensions(?string $dimensions): self
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    public function getPoids(): ?string
    {
        return $this->poids;
    }

    public function setPoids(?string $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    public function getTailleCable(): ?string
    {
        return $this->tailleCable;
    }

    public function setTailleCable(?string $taille_cable): self
    {
        $this->tailleCable = $taille_cable;

        return $this;
    }

    public function getDivers(): ?string
    {
        return $this->divers;
    }

    public function setDivers(?string $divers): self
    {
        $this->divers = $divers;

        return $this;
    }

    public function getCaracteristiquesproduits(): ?Produits
    {
        return $this->caracteristiquesproduits;
    }

    public function setCaracteristiquesproduits(?Produits $caracteristiquesproduits): self
    {
        $this->caracteristiquesproduits = $caracteristiquesproduits;

        // set (or unset) the owning side of the relation if necessary
        $newProduitscaracteristiques = null === $caracteristiquesproduits ? null : $this;
        if ($caracteristiquesproduits->getProduitscaracteristiques() !== $newProduitscaracteristiques) {
            $caracteristiquesproduits->setProduitscaracteristiques($newProduitscaracteristiques);
        }

        return $this;
    }




}
