<?php

namespace App\Entity;

use App\Repository\ProduitsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProduitsRepository::class)
 * @Vich\Uploadable()
 */
class Produits
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $marque;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="produits_images", fileNameProperty="filename")
     */
    private $imageFile;

    /**
     * @var string|null
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\ManyToMany(targetEntity=Commandes::class, inversedBy="commandesproduits", cascade={"persist", "remove"})
     */
    private $produitscommandes;


    /**
     * @ORM\ManyToOne(targetEntity=Promotions::class, inversedBy="promotionsproduits", cascade={"persist", "remove"})
     */
    private $produitspromotions;

    /**
     * @ORM\ManyToMany (targetEntity=Panier::class, inversedBy="panierproduits", cascade={"persist", "remove"})
     */
    private $produitspanier;

    /**
     * @ORM\OneToOne(targetEntity=Caracteristiques::class, inversedBy="caracteristiquesproduits", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $produitscaracteristiques;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;




    public function __construct()
    {
        $this->produitscommandes = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }


    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     * @return Produits
     */
    public function setImageFile(?File $imageFile): Produits
    {
        $this->imageFile = $imageFile;
        if ($this->imageFile instanceof UploadedFile) {
            $this->updated_at = new \DateTime('now');
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     * @return Produits
     */
    public function setFilename(?string $filename): Produits
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return Collection|Commandes[]
     */
    public function getProduitscommandes(): Collection
    {
        return $this->produitscommandes;
    }

    public function addProduitscommande(Commandes $produitscommande): self
    {
        if (!$this->produitscommandes->contains($produitscommande)) {
            $this->produitscommandes[] = $produitscommande;
        }

        return $this;
    }

    public function removeProduitscommande(Commandes $produitscommande): self
    {
        $this->produitscommandes->removeElement($produitscommande);

        return $this;
    }


    public function getProduitspromotions(): ?Promotions
    {
        return $this->produitspromotions;
    }

    public function setProduitspromotions(?Promotions $produitspromotions): self
    {
        $this->produitspromotions = $produitspromotions;

        return $this;
    }

    public function getProduitscaracteristiques(): ?Caracteristiques
    {
        return $this->produitscaracteristiques;
    }

    public function setProduitscaracteristiques(?Caracteristiques $produitscaracteristiques): self
    {
        $this->produitscaracteristiques = $produitscaracteristiques;

        return $this;
    }

    /**
     * @return Collection|Panier[]
     */
    public function getProduitspanier(): Collection
    {
        return $this->produitspanier;
    }

    public function addProduitspanier(Panier $produitspanier): self
    {
        if (!$this->produitspanier->contains($produitspanier)) {
            $this->produitspanier[] = $produitspanier;
        }

        return $this;
    }

    public function removeProduitspanier(Panier $produitspanier): self
    {
        $this->produitspanier->removeElement($produitspanier);

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }







}
