<?php
namespace App\Controller;

use App\Entity\Utilisateurs;
use App\Form\InscriptionType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ConnexionController extends AbstractController 
{

    /**
     * @var ObjectManager
     */
    private $em;
    public function __construct(EntityManagerInterface  $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/Connexion",name="Connexion.index" )
     */
    public function index(AuthenticationUtils $authenticationUtils)
    {
            $lastUserName = $authenticationUtils->getLastUsername();
            $error = $authenticationUtils->getLastAuthenticationError();
            return $this->render('Connexion/index.html.twig', [
                'last_username'=>$lastUserName,
                'error' => $error
            ]);
    }

    /**
     * @Route("/Inscription",name="Inscription.index")
     */
    public function new (Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $utilisateur = new Utilisateurs();
        $form = $this->createForm( InscriptionType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $passwordz = $passwordEncoder->encodePassword($utilisateur, $utilisateur->getPlainPassword());
            $utilisateur->setPassword($passwordz);
            $utilisateur->addRole('ROLE_USER');
            $this->em->persist($utilisateur);
            $this->em->flush();
            return $this->redirectToRoute('login');
        }

        return $this->render('Inscription/index.html.twig', [
            'utilisateur' => $utilisateur,
            'form'    => $form->createView()
        ]);
    }
}