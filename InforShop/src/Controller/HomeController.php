<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use App\Repository\ProduitsRepository;
use App\Entity\Produits;

 class HomeController extends AbstractController
{
     /**
      * @var ProduitsRepository
      */
     private $repository;

     /**
      * @var EntityManagerInterface
      */
     private $em;
     private $twig;
     public function __construct(Environment $twig,ProduitsRepository $repository, EntityManagerInterface $em)
     {
         $this->repository = $repository;
         $this->em = $em;
         $this->twig=$twig;
     }
    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(): Response
    {
        $Nouveautes = $this->repository->findNewestProducts();
        $Favoris = $this->repository->findMostSoldProducts();
        return $this->render('Pages/Home.html.twig',  [
            'Nouveautes' => $Nouveautes,
            'Favoris' => $Favoris
        ]);
    }
}