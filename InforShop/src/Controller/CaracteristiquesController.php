<?php
namespace App\Controller;

use App\Repository\CaracteristiquesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CaracteristiquesController extends AbstractController

{
    /**
     * @var CaracteristiquesRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(CaracteristiquesRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/Caracteristiques",name="Caracteristiques.index" )
     * @return Response
     */
    public function index(CaracteristiquesRepository $repository): Response
    {
        $Caracteristiquesz = $this->repository->findAll();

        return $this->render('admin/Caracteristiques/index.html.twig', [
            'Caracteristiquesz' => $Caracteristiquesz
        ]);
    }

}