<?php
namespace App\Controller;

use App\Entity\Produits;
use App\Entity\ProduitsSearch;
use App\Form\ProduitsSearchType;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController

{
    /**
     * @var ProduitsRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ProduitsRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/Produits",name="property.index" )
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $search = new ProduitsSearch();
        $form = $this->createForm(ProduitsSearchType::class, $search);
        $form->handleRequest($request);

        $produitz = $paginator->paginate(
            $this->repository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1 ),12
        );

        return $this->render('property/index.html.twig', [
            'current_menu' => 'produitz',
            'produitz' => $produitz,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/Produits/detail{id}",name="property.detail" )
     * @return Response
     */
    public function indexdetail(Produits $produitz): Response
    {
        return $this->render('property/detail.html.twig', [
            'produitz' => $produitz
        ]);
    }

}