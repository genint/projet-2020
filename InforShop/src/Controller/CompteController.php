<?php
namespace App\Controller;

use App\Entity\Utilisateurs;
use App\Entity\Commandes;
use App\Form\CommandesType;
use App\Form\InscriptionType;
use App\Repository\UtilisateursRepository;
use App\Repository\CommandesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class CompteController extends AbstractController
{
    /**
     * @var UtilisateursRepository
     */
    private $repository;
    /**
     * @var CommandesRepository
     */
    private $repositoryCm;
    /**
     * @var ObjectManager
     */
    private $em;
    private $twig;
    public function __construct(Environment $twig,UtilisateursRepository $repository, CommandesRepository $repositoryCm, EntityManagerInterface  $em)
    {
        $this->repositoryCm= $repositoryCm;
        $this->repository = $repository;
        $this->em = $em;
        $this->twig=$twig;
    }
    /**
     * @Route("/Compte", name="Compte")
     * @return Response
     */
    public function index(Request $request): Response
    {
        $user = $this->getUser()->getUsername();
        $utilisateur = $this->repository->findUserUsername($user);
        $commandes = $this->repositoryCm->findUserCommands($utilisateur);
        $form=$this->createForm(InscriptionType::class, $utilisateur);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Informations modifiées avec succès');
            return $this->redirectToRoute('Compte');
        }
        return $this->render('Compte/index.html.twig', ['utilisateur' => $utilisateur,
            'Commandes' => $commandes ,
            'form' => $form->createView()]);
    }

    /**
     * @Route("/Compte/CommandeClient/{id}", name="Compte.CommandesClient.detail")
     * @param Commandes $com
     * @return Response
     */
    public function CommandesClient(Commandes $com): Response
    {
        $prods = $com->getCommandesproduits();
        $quant = $this->repositoryCm->findProductsQuantFromCommand($com);
        return $this->render('Compte/CommandesClient/index.html.twig', ['Produits' => $prods, 'commandes' => $com, 'Quantite' => $quant]);
    }
    /**
 * @Route("/Compte/Profil", name="Compte.Profil")
 * @return Response
 */
    public function Profil(): Response
    {
        return $this->render('Compte/Profil/index.html.twig');
    }
}