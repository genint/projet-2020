<?php

namespace App\Controller;

use App\Entity\Panier;
use App\Entity\Produits;
use App\Entity\Utilisateurs;
use App\Repository\ProduitsRepository;
use App\Repository\PanierRepository;
use App\Repository\UtilisateursRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class PanierController extends AbstractController
{
    /**
     * @var UtilisateursRepository
     */
    private $repository;

    /**
     * @var ProduitsRepository
     */
    private $repositoryProduits;

    /**
     * @var PanierRepository
     */
    private $repositoryPanier;

    /**
     * @var ObjectManager
     */
    private $em;

    private $twig;
    public function __construct(Environment $twig,ProduitsRepository $repositoryProduits,UtilisateursRepository $repository, PanierRepository $repositoryPanier, EntityManagerInterface $em)
    {
        $this->repositoryPanier = $repositoryPanier;
        $this->repositoryProduits = $repositoryProduits;
        $this->repository = $repository;
        $this->em = $em;
        $this->twig=$twig;
    }
    /**
     * @Route("/Panier", name="Panier.index")
     * @return Response
     */
    public function index(): Response
    {
        $user = $this->getUser()->getUsername();
        $utilisateur = $this->repository->findUserUsername($user);
        $panier = $this->repositoryPanier->findPanierFromId($utilisateur->getId());
        if ($this->repositoryPanier->findPanierFromId($utilisateur->getId()) == null){
            $produits = new Produits();
            return $this->render('Panier/index.html.twig',  [
                'produits' => $produits
            ]);
        }
        $idpanier = $panier->getId();
        $produits = $this->repositoryProduits->findProduitsInPanier($idpanier);
        return $this->render('Panier/index.html.twig',  [
            'produits' => $produits
        ]);
    }
    /**
     * @Route("/Panier/{id}", name="Panier.add")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add($id): Response
    {
        $produit = $this->repositoryProduits->findById($id);
        $user = $this->getUser()->getUsername();
        $utilisateur = $this->repository->findUserUsername($user);
        $panieruser = $this->repositoryPanier->findPanierFromId($utilisateur->getId());
        if ($panieruser != null){
            $panieruser->addPanierproduits($produit);
            $panieruser->setPanierutilisateurs($utilisateur);
            $utilisateur->setUtilisateurspanier($panieruser);
            $this->em->persist($panieruser);

        }
        else{
            $panier = new Panier();
            $panier->addPanierproduits($produit);
            $panier->setPanierutilisateurs($utilisateur);
            $utilisateur->setUtilisateurspanier($panier);
            $this->em->persist($panier);
        }
        $this->em->flush();
        return $this->redirectToRoute('property.index');
        // Code pour un panier avec session
        /*$session = $request->getSession();
        $panier = $session->get('panier', []);
        $panier[$id] = 1;
        $session->set('panier', $panier);
        dd($session->get('panier'));*/
    }

    /**
     * @Route ("/Panier/delete/{id}", name="Panier.delete")
     */
    public function delete($id)
    {
        $produit = $this->repositoryProduits->findById($id);
        $user = $this->getUser()->getUsername();
        $utilisateur = $this->repository->findUserUsername($user);
        $panieruser = $this->repositoryPanier->findPanierFromId($utilisateur->getId());
        $panieruser->removePanierproduits($produit);
        $this->em->flush();
        return $this->redirectToRoute('Panier.index');
    }

    /**
     * @Route ("/create-checkout-session", name="checkout")
     */
    public function checkout()
    {
        $headerStringValue = $_SERVER['HTTP_NAME'];
        \Stripe\Stripe::setApiKey('sk_test_51II1aMAza6WrSbKeb7cWmQr15ojG77eSzu9OzH66IlMjgB0DAEZr0NkDvkW3FoJa0dnqsvE9hpQFLi7mQb0RgftX00BW4H7Veo');

        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price_data' => [
                    'currency' => 'eur',
                    'product_data' => [
                        'name' => $headerStringValue,
                        ],
                    'unit_amount' => 100000,
                    ],
                'quantity' => 1,
                ]],
            'mode' => 'payment',
            'success_url' => 'http://shrouded-escarpment-66579.herokuapp.com/',
            'cancel_url' => 'http://shrouded-escarpment-66579.herokuapp.com/',
        ]);
        return new JsonResponse([ 'id' => $session->id]);
    }

    /**
     * @Route ("/success", name="success")
     */
    public function succes()
    {
        return $this->render('Panier/success.html.twig');
    }
    /**
     * @Route ("/cancel", name="cancel")
     */
    public function cancel()
    {
        return $this->render('Panier/cancel.html.twig');
    }
}