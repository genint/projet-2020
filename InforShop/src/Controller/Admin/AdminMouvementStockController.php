<?php
namespace App\Controller\Admin;

use App\Entity\MouvementStock;
use App\Form\MouvementStockType;
use App\Repository\MouvementStockRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminMouvementStockController extends AbstractController
{
    /**
     * @var MouvementStockRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(MouvementStockRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/MouvementStock", name="admin.MouvementStock.index")
     * @return Response
     */
    public function index(MouvementStockRepository $repository): Response
    {
        $MouvementStockz = $this->repository->findAll();

        return $this->render('admin/MouvementStock/index.html.twig', [
            'MouvementStockz' => $MouvementStockz
        ]);
    }

    /**
     * @Route("/admin/MouvementStock/create", name="admin.MouvementStock.new")
     */
    public function new (Request $request)
    {
        $MouvementStock = new MouvementStock();
        $form = $this->createForm(MouvementStockType::class, $MouvementStock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($MouvementStock);
            $this->em->flush();
            return $this->redirectToRoute('admin.MouvementStock.index');
        }

        return $this->render('admin/MouvementStock/new.html.twig', [
            'MouvementStock' => $MouvementStock,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/MouvementStock/{id}", name="admin.MouvementStock.edit", methods="GET|POST")
     * @param MouvementStock $MouvementStock
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MouvementStock $MouvementStock, Request $request)
    {
        $form=$this->createForm(MouvementStockType::class, $MouvementStock);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'MouvementStock modifié avec succès');
            return $this->redirectToRoute('admin.MouvementStock.index');
        }
        return $this->render('admin/MouvementStock/edit.html.twig', ['MouvementStock' => $MouvementStock, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/MouvementStock/{id}",name="admin.MouvementStock.delete", methods="DELETE")
     * @param MouvementStock $MouvementStock
     */
    public function delete(MouvementStock $MouvementStock, Request $request){
        if($this->isCsrfTokenValid('delete' . $MouvementStock->getId(), $request->get('_token') )){
            $this->em->remove($MouvementStock);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.MouvementStock.index');
    }
}