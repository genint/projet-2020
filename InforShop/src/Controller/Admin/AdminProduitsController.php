<?php
namespace App\Controller\Admin;

use App\Entity\Produits;
use App\Form\ProduitsType;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminProduitsController extends AbstractController
{
    /**
     * @var ProduitsRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ProduitsRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/produit", name="admin.produit.index")
     * @return Response
     */
    public function index(ProduitsRepository $repository): Response
    {
        $produitz = $this->repository->findAll();

        return $this->render('admin/produit/index.html.twig', [
            'produitz' => $produitz
        ]);
    }

    /**
     * @Route("/admin/produit/create", name="admin.produit.new")
     */
    public function new (Request $request)
    {
        $produit = new Produits();
        $form = $this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($produit);
            $this->em->flush();
            return $this->redirectToRoute('admin.produit.index');
        }

        return $this->render('admin/produit/new.html.twig', [
            'produit' => $produit,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/produit/{id}", name="admin.produit.edit", methods="GET|POST")
     * @param Produits $produit
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Produits $produit, Request $request)
    {
        $form=$this->createForm(ProduitsType::class, $produit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Produit modifié avec succès');
            return $this->redirectToRoute('admin.produit.index');
        }
        return $this->render('admin/produit/edit.html.twig', ['produit' => $produit, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/produit/{id}",name="admin.produit.delete", methods="DELETE")
     * @param Produits $produits
     */
    public function delete(Produits $produits, Request $request){
        if($this->isCsrfTokenValid('delete' . $produits->getId(), $request->get('_token') )){
            $this->em->remove($produits);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.produit.index');
    }
}