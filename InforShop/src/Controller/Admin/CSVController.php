<?php
namespace App\Controller\Admin;

use App\Entity\Caracteristiques;
use App\Entity\CSVFile;
use App\Entity\Produits;
use App\Entity\Promotions;
use App\Form\CSVType;
use App\Form\ProduitsType;
use App\Repository\ProduitsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CSVController extends AbstractController
{
    /**
     * @var ProduitsRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ProduitsRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/CSV", name="admin.CSV.index")
     */
    public function index(Request $request)
    {
        $CSV = new CSVFile();
        $form = $this->createForm(CSVType::class, $CSV);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            /** @var UploadedFile $file */
            $file = $form->get('csvFile')->getData();
            $pathname = $file->getPathname();
            if(($handle = fopen($pathname, "r")) !== false){
                fgetcsv($handle); // Pour passer le header du .csv
                while (($CSV = fgetcsv($handle))!==false){
                    $entity = new Produits();
                    $entity2 = new Caracteristiques();
                    $entity->setFilename($CSV[0]);
                    $entity->setPrix(floatval($CSV[1]));
                    $entity->setCategorie($CSV[4]);
                    $entity->setMarque($CSV[5]);
                    $currentTime = new \DateTime('now');
                    $entity->setUpdatedAt($currentTime);
                    $entity2->setNumeroModele($CSV[6]);
                    $entity2->setSerie($CSV[7]);
                    $entity2->setCouleur($CSV[8]);
                    $entity2->setGarantieConstructeur($CSV[9]);
                    $entity2->setSystemeExploitation($CSV[10]);
                    $entity2->setDescrptionClavier($CSV[11]);
                    $entity2->setMarqueProcesseur($CSV[12]);
                    $entity2->setTypeProcesseur($CSV[13]);
                    $entity2->setVitesseProcesseur($CSV[14]);
                    $entity2->setNbrCoeurs(intval($CSV[15]));
                    $entity2->setTailleMemoireVive($CSV[16]);
                    $entity2->setTailleDisqueDur($CSV[17]);
                    $entity2->setTechnologieDisqueDur($CSV[18]);
                    $entity2->setTypeEcran($CSV[19]);
                    $entity2->setTailleEcran($CSV[20]);
                    $entity2->setResEcran($CSV[21]);
                    $entity2->setResMaxEcran($CSV[22]);
                    $entity2->setDescripCarteGraphique($CSV[23]);
                    $entity2->setGPU($CSV[24]);
                    $entity2->setGPURam($CSV[25]);
                    $entity2->setTypeRamCarteGraphique($CSV[26]);
                    $entity2->setConnectivite($CSV[27]);
                    $entity2->setBluetooth($CSV[28]);
                    $entity2->setNbrPortsHDMI(intval($CSV[29]));
                    $entity2->setNbrPortsVGA(intval($CSV[30]));
                    $entity2->setNbrPortsUSB2(intval($CSV[31]));
                    $entity2->setNbrPortsUSB3(intval($CSV[32]));
                    $entity2->setNbrPortsEthernet(intval($CSV[33]));
                    $entity2->setTypeConnecteur($CSV[34]);
                    $entity2->setDimensions($CSV[35]);
                    $entity2->setPoids($CSV[36]);
                    $entity2->setTailleCable($CSV[37]);
                    $entity2->setDivers($CSV[38]);
                    $entity->setProduitscaracteristiques($entity2);
                    $entity2->setCaracteristiquesproduits($entity);
                    $this->em->persist($entity);
                    $this->em->persist($entity2);
                    if (intval($CSV[2])){
                        $entity3 = new Promotions();
                        $entity3->setRemise(intval($CSV[2]));
                        $entity3->setDelai(intval($CSV[3]));
                        $entity->setProduitspromotions($entity3);
                        $this->em->persist($entity3);
                    }
                }
            }
            fclose($handle);
            $this->em->flush();
            return $this->redirectToRoute('admin.CSV.index');
        }

        return $this->render('admin/CSV/index.html.twig', ['form'    => $form->createView()]);
    }
}