<?php
namespace App\Controller\Admin;

use App\Entity\Promotions;
use App\Form\PromotionsType;
use App\Repository\PromotionsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminPromotionsController extends AbstractController
{
    /**
     * @var PromotionsRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(PromotionsRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/Promotions", name="admin.Promotions.index")
     * @return Response
     */
    public function index(PromotionsRepository $repository): Response
    {
        $Promotionsz = $this->repository->findAll();

        return $this->render('admin/Promotions/index.html.twig', [
            'Promotionsz' => $Promotionsz
        ]);
    }

    /**
     * @Route("/admin/Promotions/create", name="admin.Promotions.new")
     */
    public function new (Request $request)
    {
        $Promotions = new Promotions();
        $form = $this->createForm(PromotionsType::class, $Promotions);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($Promotions);
            $this->em->flush();
            return $this->redirectToRoute('admin.Promotions.index');
        }

        return $this->render('admin/Promotions/new.html.twig', [
            'Promotions' => $Promotions,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/Promotions/{id}", name="admin.Promotions.edit", methods="GET|POST")
     * @param Promotions $Promotions
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Promotions $Promotions, Request $request)
    {
        $form=$this->createForm(PromotionsType::class, $Promotions);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Promotion modifié avec succès');
            return $this->redirectToRoute('admin.Promotions.index');
        }
        return $this->render('admin/Promotions/edit.html.twig', ['Promotions' => $Promotions, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/Promotions/{id}",name="admin.Promotions.delete", methods="DELETE")
     * @param Promotions $Promotions
     */
    public function delete(Promotions $Promotions, Request $request){
        if($this->isCsrfTokenValid('delete' . $Promotions->getId(), $request->get('_token') )){
            $this->em->remove($Promotions);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.Promotions.index');
    }
}