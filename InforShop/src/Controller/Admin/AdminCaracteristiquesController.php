<?php
namespace App\Controller\Admin;

use App\Entity\Caracteristiques;
use App\Form\CaracteristiquesType;
use App\Repository\CaracteristiquesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCaracteristiquesController extends AbstractController
{
    /**
     * @var CaracteristiquesRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(CaracteristiquesRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/Caracteristiques", name="admin.Caracteristiques.index")
     * @return Response
     */
    public function index(CaracteristiquesRepository $repository): Response
    {
        $Caracteristiquesz = $this->repository->findAll();

        return $this->render('admin/Caracteristiques/index.html.twig', [
            'Caracteristiquesz' => $Caracteristiquesz
        ]);
    }

    /**
     * @Route("/admin/Caracteristiques/create", name="admin.Caracteristiques.new")
     */
    public function new (Request $request)
    {
        $Caracteristiques = new Caracteristiques();
        $form = $this->createForm(CaracteristiquesType::class, $Caracteristiques);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($Caracteristiques);
            $this->em->flush();
            return $this->redirectToRoute('admin.Caracteristiques.index');
        }

        return $this->render('admin/Caracteristiques/new.html.twig', [
            'Caracteristiques' => $Caracteristiques,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/Caracteristiques/{id}", name="admin.Caracteristiques.edit", methods="GET|POST")
     * @param Caracteristiques $Caracteristiques
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Caracteristiques $Caracteristiques, Request $request)
    {
        $form=$this->createForm(CaracteristiquesType::class, $Caracteristiques);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Caracteristiques modifié avec succès');
            return $this->redirectToRoute('admin.Caracteristiques.index');
        }
        return $this->render('admin/Caracteristiques/edit.html.twig', ['Caracteristiques' => $Caracteristiques, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/Caracteristiques/{id}",name="admin.Caracteristiques.delete", methods="DELETE")
     * @param Caracteristiques $Caracteristiques
     */
    public function delete(Caracteristiques $Caracteristiques, Request $request){
        if($this->isCsrfTokenValid('delete' . $Caracteristiques->getId(), $request->get('_token') )){
            $this->em->remove($Caracteristiques);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.Caracteristiques.index');
    }
}