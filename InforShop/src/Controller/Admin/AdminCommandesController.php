<?php
namespace App\Controller\Admin;

use App\Entity\Commandes;
use App\Form\CommandesType;
use App\Repository\CommandesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCommandesController extends AbstractController
{
    /**
     * @var CommandesRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(CommandesRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/Commandes", name="admin.Commandes.index")
     * @return Response
     */
    public function index(CommandesRepository $repository): Response
    {
        $Commandesz = $this->repository->findAll();

        return $this->render('admin/Commandes/index.html.twig', [
            'Commandesz' => $Commandesz
        ]);
    }

    /**
     * @Route("/admin/Commandes/create", name="admin.Commandes.new")
     */
    public function new (Request $request)
    {
        $Commandes = new Commandes();
        $form = $this->createForm(CommandesType::class, $Commandes);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($Commandes);
            $this->em->flush();
            return $this->redirectToRoute('admin.Commandes.index');
        }

        return $this->render('admin/Commandes/new.html.twig', [
            'Commandes' => $Commandes,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/Commandes/{id}", name="admin.Commandes.edit", methods="GET|POST")
     * @param Commandes $Commandes
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Commandes $Commandes, Request $request)
    {
        $form=$this->createForm(CommandesType::class, $Commandes);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Commandes modifié avec succès');
            return $this->redirectToRoute('admin.Commandes.index');
        }
        return $this->render('admin/Commandes/edit.html.twig', ['Commandes' => $Commandes, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/Commandes/{id}",name="admin.Commandes.delete", methods="DELETE")
     * @param Commandes $Commandes
     */
    public function delete(Commandes $Commandes, Request $request){
        if($this->isCsrfTokenValid('delete' . $Commandes->getId(), $request->get('_token') )){
            $this->em->remove($Commandes);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.Commandes.index');
    }
}