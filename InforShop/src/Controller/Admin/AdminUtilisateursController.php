<?php
namespace App\Controller\Admin;

use App\Entity\Utilisateurs;
use App\Form\UtilisateursType;
use App\Repository\UtilisateursRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminUtilisateursController extends AbstractController
{
    /**
     * @var UtilisateursRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(UtilisateursRepository $repository, EntityManagerInterface  $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/utilisateurs", name="admin.utilisateurs.index")
     * @return Response
     */
    public function index(UtilisateursRepository $repository): Response
    {
        $utilisateurz = $this->repository->findAll();

        return $this->render('admin/utilisateurs/index.html.twig', [
            'utilisateurz' => $utilisateurz
        ]);
    }

    /**
     * @Route("/admin/utilisateurs/create", name="admin.utilisateur.new")
     */
    public function new (Request $request)
    {
        $utilisateur = new Utilisateurs();
        $form = $this->createForm(UtilisateursType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($utilisateur);
            $this->em->flush();
            return $this->redirectToRoute('admin.utilisateurs.index');
        }

        return $this->render('admin/utilisateurs/new.html.twig', [
            'utilisateur' => $utilisateur,
            'form'    => $form->createView()
        ]);

    }
    /**
     * @Route("/admin/utilisateurs/{id}", name="admin.utilisateur.edit", methods="GET|POST")
     * @param Utilisateurs $utilisateur
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Utilisateurs $utilisateur, Request $request)
    {
        $form=$this->createForm(utilisateursType::class, $utilisateur);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->em->flush();
            $this->addFlash('success', 'Utilisateur modifié avec succès');
            return $this->redirectToRoute('admin.utilisateurs.index');
        }
        return $this->render('admin/utilisateurs/edit.html.twig', ['utilisateur' => $utilisateur, 'form' => $form->createView()]);
    }

    /**
     * @Route("admin/utilisateurs/{id}",name="admin.utilisateur.delete", methods="DELETE")
     * @param Utilisateurs $utilisateurs
     */
    public function delete(Utilisateurs $utilisateurs, Request $request){
        if($this->isCsrfTokenValid('delete' . $utilisateurs->getId(), $request->get('_token') )){
            $this->em->remove($utilisateurs);
            $this->em->flush();
        }
        return $this->redirectToRoute('admin.utilisateurs.index');
    }
}
