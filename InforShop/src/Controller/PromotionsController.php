<?php
namespace App\Controller;

use App\Repository\PromotionsRepository;
use App\Entity\Promotions;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PromotionsController extends AbstractController

{
    /**
     * @var PromotionsRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(PromotionsRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/Promotions",name="Promotions.index" )
     * @return Response
     */
    public function index(): Response
    {
        $promotions = $this->repository->findAll();

        return $this->render('Promotions/index.html.twig',  [
            'promotions' => $promotions
        ]);
    }

}
